import Config

config :logger, :console,
  metadata: [:file, :mfa]
