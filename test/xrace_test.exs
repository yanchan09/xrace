defmodule XraceTest do
  use ExUnit.Case

  test "get_or_create returns same client with same addr" do
    client1 = XRace.ClientRegistry.get_or_create(XRace.ClientRegistry, :test_addr)
    client2 = XRace.ClientRegistry.get_or_create(XRace.ClientRegistry, :test_addr)
    assert client1 == client2
  end

  test "get_or_create returns different clients with different addrs" do
    client1 = XRace.ClientRegistry.get_or_create(XRace.ClientRegistry, :test_addr)
    client2 = XRace.ClientRegistry.get_or_create(XRace.ClientRegistry, :test_addr_b)
    assert client1 != client2
  end

  test "clients know their address" do
    client = XRace.ClientRegistry.get_or_create(XRace.ClientRegistry, :test_addr)
    assert XRace.Client.whoami(client) == :test_addr
  end

  test "clients are removed on exit" do
    client = XRace.ClientRegistry.get_or_create(XRace.ClientRegistry, :test_addr)
    Agent.stop(client)
    client = XRace.ClientRegistry.get_or_create(XRace.ClientRegistry, :test_addr_b)
    assert XRace.Client.whoami(client) == :test_addr_b
  end
end
