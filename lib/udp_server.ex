defmodule XRace.UDPServer do
  require Logger

  def listen(port) when is_integer(port) do
    {:ok, socket} = :gen_udp.open(port, [:binary, active: false])

    receive_packet_loop(socket)
  end

  def receive_packet_loop(socket) do
    {:ok, {addr, port, data}} = :gen_udp.recv(socket, 4096)
    Logger.debug("Received packet from #{inspect(addr)}:#{port}: #{data}")

    client = XRace.ClientRegistry.get_or_create(XRace.ClientRegistry, {addr, port})
    XRace.Client.handle_packet(client, data)

    receive_packet_loop(socket)
  end
end
