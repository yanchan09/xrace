defmodule XRace.PacketDecoder do
  @doc ~S"""
  Decodes packet sent by a client

  ## Examples

      iex> XRace.PacketDecoder.decode_packet(<<0, 0, 0, 6, 1, 10, 11, 12, 13, 0, 0, 39, 16, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 254, 10, 69, 162, 86, 49>>)
      {:ok, 0,
        {:hello,
          %{
            hello_time: 65034,
            iv: <<0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
              0, 0, 0, 0, 0, 0, 0, 0, 0>>,
            ticket: <<1, 10, 11, 12, 13, 0, 0, 39, 16, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
              0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0>>
          }}}

      iex> XRace.PacketDecoder.decode_packet(<<0, 0, 1, 7, 2, 254, 71, 1, 222, 255, 255, 255, 255, 0, 6, 0, 0, 0, 39, 16, 36, 255, 198, 184, 186, 212>>)
      {:ok, 1,
        {:sync,
          %{
            client_time: 65095,
            server_time: 478,
            sync_byte: 65535,
            sync_counter: 65535
          }, {:start, %{player_count: 2, session_id: 10000, slot: 1}}}}

      iex> XRace.PacketDecoder.decode_packet(<<1, 0, 0, 39, 1, 230, 1, 250, 255, 255, 18, 26, 56, 198, 152, 9, 22, 173, 199, 84, 48, 51, 136, 75, 19, 136, 64, 57, 151, 238, 247, 45, 45, 45, 52, 80, 0, 127, 255, 45, 208, 75, 39, 255>>)
      {:ok, :p2p,
        [
          {0,
            <<1, 230, 1, 250, 255, 255, 18, 26, 56, 198, 152, 9, 22, 173, 199, 84, 48,
              51, 136, 75, 19, 136, 64, 57, 151, 238, 247, 45, 45, 45, 52, 80, 0, 127,
              255, 45>>}
        ]}

  Invalid packets return an error:

      iex> XRace.PacketDecoder.decode_packet(<<>>)
      {:error, :invalid_packet_size}

      iex> XRace.PacketDecoder.decode_packet(<<0, 0, 0, 0, 0>>)
      {:error, :invalid_control_packet}

      iex> XRace.PacketDecoder.decode_packet(<<255, 0, 0, 0, 0>>)
      {:error, :unknown_packet_type}

  """
  def decode_packet(bytes) do
    len = byte_size(bytes)
    if len < 5 do
      {:error, :invalid_packet_size}
    else
      bytes_without_crc = binary_part(bytes, 0, len - 4)
      case bytes_without_crc do
        <<0, rest::binary>> -> decode_control_packet(rest)
        <<1, rest::binary>> -> decode_peer_packet(rest)
        _ -> {:error, :unknown_packet_type}
      end
    end
  end

  def decode_control_packet(bytes) do
    case bytes do
      <<seq::16, 0x06,
        ticket::binary-size(32), iv::binary-size(32), 0x00,
        hello_time::16>> -> {:ok, seq, {:hello, %{
          ticket: ticket,
          iv: iv,
          hello_time: hello_time,
        }}}
      <<seq::16, 0x07, 0x02, payload::binary>> -> _decode_sync_packet(seq, payload)
      _ -> {:error, :invalid_control_packet}
    end
  end

  def _decode_sync_packet(seq, bytes) do
    case bytes do
      <<
        client_time::16,
        server_time::16,
        counter::16,
        sync::16,
        subpacket::binary
      >> ->
        header = %{
          client_time: client_time,
          server_time: server_time,
          sync_counter: counter,
          sync_byte: sync
        }
        case _decode_sync_subpacket(subpacket) do
          {:ok, sp} -> {:ok, seq, {:sync, header, sp}}
          :error -> {:error, :invalid_sync_subpacket}
        end
      _ -> {:error, :invalid_sync_packet}
    end
  end

  def _decode_sync_subpacket(bytes) do
    case bytes do
      <<0xff>> ->
        {:ok, :keep_alive}

      <<0x00, 0x06, 0x00, session_id::32,
          slot::little-integer-size(3), 0::size(1), player_count::little-integer-size(3), 0::size(1),
          0xff>> ->
        {:ok, {:start, %{session_id: session_id, slot: slot, player_count: player_count}}}

      <<0x01, 0x02, _unknown::16, 0xff>> ->
        {:ok, :sync}

      _ -> :error
    end
  end

  def decode_peer_packet(bytes) do
    _decode_peer_packet([], bytes)
  end

  def _decode_peer_packet(list, bytes) do
    size = byte_size(bytes)
    if size > 0 do
      case _decode_peer_message(bytes) do
        {:ok, skipped_bytes, msg} ->
          _decode_peer_packet(list ++ [msg], binary_part(bytes, skipped_bytes, size-skipped_bytes))
        err -> err
      end
    else
      {:ok, :p2p, list}
    end
  end

  def _decode_peer_message(bytes) do
    case bytes do
      <<peer_id, msg_size::16, rest::binary>> ->
        cond do
          byte_size(rest) < (msg_size - 3) ->
            {:error, :invalid_peer_msg_size}
          true -> {:ok, msg_size, {peer_id, binary_part(rest, 0, msg_size - 3)}}
        end
      _ -> {:error, :invalid_peer_msg}
    end
  end
end
