defmodule XRace.Client do
  use GenServer, restart: :temporary

  def start_link(client_opts) do
    GenServer.start_link(__MODULE__, client_opts)
  end

  def whoami(server) do
    GenServer.call(server, :whoami)
  end

  def handle_packet(server, data) do
    GenServer.cast(server, {:handle_packet, data})
  end

  @impl true
  def init(opts) do
    {:ok, %{
      :addr => opts[:addr]
    }}
  end

  @impl true
  def handle_call(:whoami, _from, state) do
    {:reply, state[:addr], state}
  end

  @impl true
  def handle_cast({:handle_packet, data}, state) do
    require Logger
    Logger.debug("[#{inspect(state[:addr])}] Received packet: #{data}")
    {:noreply, state}
  end
end
