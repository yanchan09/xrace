defmodule XRace.ClientRegistry do
  use GenServer

  def start_link(_opts) do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def get_or_create(server, addr) do
    GenServer.call(server, {:get_or_create, addr})
  end

  @impl true
  def init(:ok) do
    addrs = %{}
    refs = %{}
    {:ok, {addrs, refs}}
  end

  @impl true
  def handle_call({:get_or_create, addr}, _from, {addrs, refs}) do
    case Map.fetch(addrs, addr) do
      {:ok, client} -> {:reply, client, {addrs, refs}}
      :error ->
        {:ok, pid} = DynamicSupervisor.start_child(XRace.ClientSupervisor, {XRace.Client, addr: addr})
        ref = Process.monitor(pid)
        addrs = Map.put(addrs, addr, pid)
        refs = Map.put(refs, ref, addr)
        {:reply, pid, {addrs, refs}}
    end
  end

  @impl true
  def handle_info({:DOWN, ref, :process, _pid, _reason}, {addrs, refs}) do
    {addr, refs} = Map.pop(refs, ref)
    addrs = Map.delete(addrs, addr)
    {:noreply, {addrs, refs}}
  end

  @impl true
  def handle_info(msg, state) do
    require Logger
    Logger.debug("Unexpected message in #{__MODULE__}: #{inspect(msg)}")
    {:noreply, state}
  end
end
