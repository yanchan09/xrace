defmodule XRace do
  use Application

  @impl true
  def start(_type, _args) do
    XRace.Supervisor.start_link(name: XRace.Supervisor)
  end
end
