defmodule XRace.Supervisor do
  use Supervisor

  def start_link(opts) do
    Supervisor.start_link(__MODULE__, :ok, opts)
  end

  @impl true
  def init(:ok) do
    children = [
      XRace.ClientRegistry,
      {DynamicSupervisor, name: XRace.ClientSupervisor, strategy: :one_for_one},
      Supervisor.child_spec({Task, fn -> XRace.UDPServer.listen(9988) end}, restart: :permanent)
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end
end
